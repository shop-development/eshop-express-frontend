export type ArticleTypes = {
    id?: number;
    image: string;
    name:string;
    description:string;
    amount :number;
    price : number;
    articleId: number;
    category?:string;
}

export type ArticleReducerState = {
    articles : Array<ArticleTypes>,
    status:string;
}


export type CategorieType = {
    categorie:string;
}

export type SettingsReducerState = {
    categories: Array<CategorieType> ;
}


