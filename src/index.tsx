import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './components/header/NavigationItem.scss'
import './components/header/HeaderWrapperStyles.scss'
import './components/logo/LogoStyles.scss'
import './components/filter-bar/FilterBarStyles.scss'
import './components/filter-bar/filter-item/FilterItemStyles.scss'
import './components/shop-article/ShopArticleStyles.scss'
import './components/shop-area/ShopAreaStyles.scss'
import {Provider} from "react-redux";
import store from './redux-modules'
import './components/long-text-cutter/LongTextCutterStyles.scss'
import './components/cart/CartStyles.scss'
import './components/cart/cart-article/CartArticle.scss'
import './components/input/InputStyles.scss'

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
        <Provider store={store}>
            <App/>
        </Provider>
    </React.StrictMode>,
)
