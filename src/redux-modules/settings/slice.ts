import {createSlice} from "@reduxjs/toolkit";
import {SettingsReducerState} from "../../types/ArticleTypes.ts";
import {getArticleFilters} from "./action.ts";

const initialState: SettingsReducerState = {
    categories: []
}

const settingsSlice = createSlice({
    name: "settingsSLice",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getArticleFilters.fulfilled, (state, action) => {
            state.categories = action.payload.categories
        })
    }
})

export const settingsReducer = settingsSlice.reducer;