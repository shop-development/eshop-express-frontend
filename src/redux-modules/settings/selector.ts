import {RootState} from "../index.ts";

export const selectSettingsState = (state:RootState) => state.settings

export const selectFilterCategories = (state:RootState) => selectSettingsState(state).categories