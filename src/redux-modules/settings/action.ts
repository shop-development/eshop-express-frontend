import {createAsyncThunk} from "@reduxjs/toolkit";
import {SettingsReducerState} from "../../types/ArticleTypes.ts";
import {SHOP_BACKEND_URL} from "../../constant/url.ts";

export const getArticleFilters = createAsyncThunk<SettingsReducerState>('filter/getArticleFilter', async () => {

    const response = await fetch(`${SHOP_BACKEND_URL}settings/articleFilter`, {
        method: 'GET',
    })

    if (response.status >= 300) {
        throw new Error('Fehler beim laden der FilterItems!')
    }

    return response.json();

})