import {combineReducers, configureStore} from "@reduxjs/toolkit";
import {articleReducer} from "./article/slice.ts";
import {settingsReducer} from "./settings/slice.ts";


const rootReducer = combineReducers({
    articles: articleReducer,
    settings: settingsReducer
})

const Store = configureStore({
    reducer: rootReducer,

})

export type RootState = ReturnType<typeof rootReducer>;
export type AppDispatch = typeof Store.dispatch;
export default Store;


