import {createSlice} from "@reduxjs/toolkit";
import {ArticleReducerState} from "../../types/ArticleTypes.ts";
import {getAllArticle, getCategorieArticle} from "./action.ts";

const initialState: ArticleReducerState = {
    articles: [],
    status: 'initial'
};

const articleSlice = createSlice({
    name: 'articleSlice',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder.addCase(getAllArticle.fulfilled, (state, action) => {
            state.articles = action.payload
            state.status = 'fulfilled';
        })

        builder.addCase(getAllArticle.rejected, (state) => {
            state.articles = []
            state.status = 'rejected'
        })

        builder.addCase(getCategorieArticle.fulfilled, (state, action) => {
            state.articles = action.payload;
            state.status = 'fulfilled'
        });
        builder.addCase(getCategorieArticle.rejected, (state) => {
            state.articles = [];
            state.status = 'rejected'
        })

    }
})

export const articleReducer = articleSlice.reducer;