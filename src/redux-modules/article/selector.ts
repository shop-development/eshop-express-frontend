import {RootState} from "../index.ts";

export const selectArticleState = (state:RootState) => state.articles;

export const selectArticles = (state:RootState) => selectArticleState(state).articles;