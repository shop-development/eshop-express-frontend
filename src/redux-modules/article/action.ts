import {createAsyncThunk} from "@reduxjs/toolkit";
import {ArticleTypes} from "../../types/ArticleTypes.ts";
import {SHOP_BACKEND_URL} from "../../constant/url.ts";

export const getAllArticle = createAsyncThunk<ArticleTypes[]>('article/getAllArticle', async () => {

    const response = await fetch(`${SHOP_BACKEND_URL}Shop/GetAllArticles`, {
        method: 'GET'
    })


    if (response.status >= 300) {
        throw new Error("Beim laden der Artikel ist ein Fehler aufgetreten!")
    }

    return response.json();

})

export const getCategorieArticle = createAsyncThunk<ArticleTypes[], { category: string }>('article/getCategoryArticle', async ({category}) => {

    const response = await fetch(`${SHOP_BACKEND_URL}Shop/GetArticlesByCategorie?categorie=${category}`, {
        method: 'GET'
    })

    if (response.status > 300) {
        throw new Error(`Fehler beim Laden der Artikel der ${category} Kategorie!`)
    }

    return response.json();

})

