import {FunctionComponent, ReactNode} from "react";

type NavigationItemProps = {
    href:string;
    children:string | ReactNode;
    fontColor?:string | undefined;
}

const NavigationItem:FunctionComponent<NavigationItemProps> = ({href, children,fontColor}) =>{

    if(!fontColor){
        fontColor = "black"
    }

    return(

        <div className="navigation-item-container">
            <a style={{color:`${fontColor }`}} className="navigation-item" href={href}>{children}</a>
        </div>

)


}

export default NavigationItem;