import Logo from "../logo/Logo.tsx";
import styled from "styled-components";

const LogoContainer = styled(Logo)`
  height: 100px;
  width: 100px;`
const HeaderWrapper = () => {

    return (
        <div className="header-wrapper">

            <div className="header-logo-container">
                <LogoContainer src='../images/E-ShopExpress LogoCopie.png'/></div>
            <div className="header-headline">
                E-Shop-Express
            </div>
            <div className="header-navigation-container"></div>
            <div className="header-right-elements"></div>


        </div>
    )


}

export default HeaderWrapper;