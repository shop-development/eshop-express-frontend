import React, {FunctionComponent, ReactNode} from "react";

export type FilterItemProps = {
    className?: string;
    onClick?: (event: React.MouseEvent<HTMLElement>) => void | Promise<void>
    children: string | ReactNode;
    category?: string;
}

const FilterItem: FunctionComponent<FilterItemProps> = ({category, onClick, className, children}) => {

    return (
        <div key={category} className={`filter-item-styles ${className}`} onClick={onClick}>
            {children}
        </div>
    )
}


export default FilterItem;