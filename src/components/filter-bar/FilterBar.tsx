import FilterItem from "./filter-item/FilterItem.tsx";
import {FunctionComponent, useCallback, useEffect} from "react";
import {useAppDispatch, useAppSelector} from "../../hooks/store.ts";
import {getAllArticle, getCategorieArticle} from "../../redux-modules/article/action.ts";
import {getArticleFilters} from "../../redux-modules/settings/action.ts";
import {selectFilterCategories} from "../../redux-modules/settings/selector.ts";

type FilterBarProps = {
    className?: string;
    headline: string;
}

const FilterBar: FunctionComponent<FilterBarProps> = ({className, headline}) => {


    const dispatch = useAppDispatch();

    const filterItems = useAppSelector((state) => selectFilterCategories(state))

    const uniqueCategories = new Set(filterItems.map(item => item.categorie));

    const uniqueFilterItems = filterItems.filter(item => {
        if (uniqueCategories.has(item.categorie)) {
            uniqueCategories.delete(item.categorie);
            return true;
        }
        return false;
    });

    const handleGetAllCategorys = useCallback(async () => {
        await dispatch(getAllArticle())
    }, [dispatch])

    const handleFilterCategory = useCallback(async (value: string) => {
        await dispatch(getCategorieArticle({category: value}))
    }, [dispatch])


    useEffect(() => {
        dispatch(getArticleFilters())
    }, [dispatch]);

    return (
            <div className={`filter-bar-parent-container ${className}`}>
                <div className={`filter-bar-headline`}>
                    {headline}
                </div>
                <div className={`filter-items-flex-container`}>
                    <FilterItem onClick={handleGetAllCategorys}>Alle</FilterItem>
                    {uniqueFilterItems.map((item) => (
                        <FilterItem
                            onClick={() => handleFilterCategory(item.categorie ? item.categorie : '')}>{item.categorie}</FilterItem>
                    ))}
                </div>
            </div>
    )

}

export default FilterBar;