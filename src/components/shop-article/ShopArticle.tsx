import {FunctionComponent, useMemo} from "react";
import {ArticleTypes} from "../../types/ArticleTypes.ts";
import LongTextCutter from "../long-text-cutter/LongTextCutter.tsx";

const ShopArticle: FunctionComponent<ArticleTypes> = ({articleId, image, name, price, description, amount}) => {

    const checkIfNoArticle = useMemo(() => amount === 0, [amount]);

    return (
        <div key={articleId} className="article-container">

            <div className="article-image-container">
                <img className="article-image" src={image} alt="Bild eines Shop-Artikels!"/>
            </div>

            <div className="article-information">

                <div className="article-name article-property">

                    <p>{name}</p>

                </div>

                <div className="article-description article-property">
                    {
                        description.length > 50 ? (
                            <LongTextCutter text={description} maxLength={24}/>
                        ) : (
                            description
                        )
                    }
                </div>

                <div className="article-amount article-property">
                    {checkIfNoArticle && (
                        <p>Ausverkauft!</p>
                    )}

                </div>

                <div className="article-price article-property">

                    <button disabled={checkIfNoArticle} className="article-price-font article-property">{price}€
                    </button>

                </div>

            </div>

        </div>
    )

}


export default ShopArticle;