import {FunctionComponent, MouseEventHandler} from "react";
import './ButtonStyles.scss'

type ButtonProps = {
    children: string;
    onClick?: MouseEventHandler<HTMLButtonElement>
}

const Button: FunctionComponent<ButtonProps> = ({onClick, children}) => {


    return (

        <button onClick={onClick}>{children}</button>

    )


}


export default Button;