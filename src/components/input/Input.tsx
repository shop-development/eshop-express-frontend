import {ChangeEventHandler, FunctionComponent} from "react";

type InputProps = {
    type?: string,
    placeholder?: string;
    className?: string;
    onChange?: ChangeEventHandler<HTMLInputElement>
}


const Input: FunctionComponent<InputProps> = ({onChange, className, placeholder, type}) => {


    return (

        <input type={type} className={`input-component-styles ${className}`} onChange={onChange}
               placeholder={placeholder}/>


    )


}
export default Input;