import {FunctionComponent, useState} from 'react';

type LongTextCutterProps = {
    text: string;
    maxLength: number;
}

const LongTextCutter: FunctionComponent<LongTextCutterProps> = ({text, maxLength}) => {
    const [showFullText, setShowFullText] = useState(false);

    const shortenText = (text: string, maxLength: number) => {
        if (text.length <= maxLength) {
            return text;
        }
        return text.slice(0, maxLength) + '...';
    };

    const toggleFullText = () => {
        setShowFullText(!showFullText);
    };

    return (
        <div>
            {showFullText ? (
                <div className="description-direction">
                    {text}
                    <div className="long-text-cutter-styles" onClick={toggleFullText}>Weniger anzeigen</div>
                </div>
            ) : (
                <div className="description-direction">
                    {shortenText(text, maxLength)}
                    <div className="long-text-cutter-styles" onClick={toggleFullText}>Mehr anzeigen</div>
                </div>
            )}
        </div>
    );
};

export default LongTextCutter;