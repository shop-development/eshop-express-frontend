import ShopArticle from "../shop-article/ShopArticle.tsx";
import {useAppDispatch, useAppSelector} from "../../hooks/store.ts";
import {selectArticles} from "../../redux-modules/article/selector.ts";
import {useEffect} from "react";
import {getAllArticle} from "../../redux-modules/article/action.ts";


const ShopArea = () => {

    const articleList = useAppSelector((state) => selectArticles(state))

    const dispatch = useAppDispatch();

    useEffect(() => {
        dispatch(getAllArticle())
    }, [dispatch]);


    return (
        <>
            {articleList.map((item) => (
                <ShopArticle image={item.image} name={item.name} description={item.description} amount={item.amount}
                             price={item.price} articleId={item.articleId}/>
            ))}

        </>

    )

}


export default ShopArea;