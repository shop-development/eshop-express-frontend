import CartArticle from "./cart-article/CartArticle.tsx";
import Input from "../input/Input.tsx";
import {useCallback, useState} from "react";
import styled from "styled-components";
import Button from "../button/Button.tsx";


const InputComponent = styled(Input)`
  background: none;
  border: none;
  border-bottom: 2px solid #5e5e5e;
  border-radius: 0`


const Cart = () => {

    const [showOrderConfirmation, setShowOrderConfirmation] = useState(false)

    const handleShowOrderConfirmation = useCallback(() => {
        if (showOrderConfirmation) {
            setShowOrderConfirmation(false)
        } else {
            setShowOrderConfirmation(true)
        }
    }, [showOrderConfirmation])


    return (
        <div className="cart-wrapper">

            <div className="cart-headline-wrapper">
                <h1 className="cart-headline">Bestellung abschließen</h1>
            </div>


            <div className="cart-articles-wrapper">

                <div className="cart-articles">
                    <CartArticle name="Apfel" amount={3} price={4.49}/>
                </div>
            </div>


            <div className="cart-order-informations">


                <div className="cart-settings">

                    <div>
                        <label>Zeitpunkt auswählen</label>
                    </div>


                    <InputComponent placeholder="Zeitpunkt"/>

                </div>


                <div className="cart-settings">

                    <div>
                        <label>Lieferadresse</label>
                    </div>

                    <InputComponent placeholder="Adresse"/>
                </div>


                <div className="cart-settings">

                    <div>
                        <label>Kommentar</label>
                    </div>

                    <InputComponent placeholder="Kommentar hinzufügen"/>

                </div>

                <div className="cart-settings">
                    <div>
                        <label>Zahlungsmöglichkeit auswählen</label>
                    </div>

                    <select>
                        <option>
                            Zahlungsmöglichkeit auswählen
                        </option>
                        <option>PayPal</option>
                        <option>Klarna</option>
                        <option>Banküberweisung</option>
                    </select>
                </div>

                <div className="cart-settings">
                    <div>
                        <label>Bestellbestätigung per E-Mail erhalten</label>
                        <input type="checkbox" onChange={handleShowOrderConfirmation}/>
                    </div>
                    {showOrderConfirmation && (
                        <InputComponent placeholder="E-Mail Adresse"/>
                    )}
                </div>
            </div>


            <div className="finish-order-wrapper">


                <div className="finish-order-button">
                    <Button>Bestellung aufgeben</Button>
                </div>

            </div>


        </div>
    )

}

export default Cart;