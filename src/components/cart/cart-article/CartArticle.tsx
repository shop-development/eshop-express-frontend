import {FunctionComponent} from "react";

type CartArticleProps = {
    name: string;
    amount: number;
    price: number;
}
const CartArticle: FunctionComponent<CartArticleProps> = ({name, amount, price}) => {

    const totalArticlePrice = price * amount;

    return (

        <div className="cart-article-container">

            <div className="cart-article-header">

                <div>Name</div>
                <div>Anzahl</div>
                <div>Preis pro Stück</div>
                <div>gesamt</div>

            </div>

            <div className="cart-article-item">
                <div>
                    {name}
                </div>

                <div>
                    {`${amount}x`}
                </div>


                <div>
                    {`${price} €`}
                </div>

                <div>
                    {`${totalArticlePrice} €`}
                </div>

            </div>
        </div>
    )
}

export default CartArticle;