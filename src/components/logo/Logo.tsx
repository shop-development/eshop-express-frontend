import {FunctionComponent} from "react";

type LogoProps = {
    src: string;
    className?: string;
}

const Logo: FunctionComponent<LogoProps> = ({
                                                src,
                                                className
                                            }) => {

    return (

        <div className={`logo-container ${className}`}>
            <img style={{height: '100%', width: '100%;' ,imageOrientation: 'auto'}} src={src}/>
        </div>

    )
}

export default Logo;